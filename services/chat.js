// Watson
const watson = require('watson-developer-cloud')

const conversation = new watson.AssistantV1({
  version: '2018-04-03',
  username: process.env.CONVERSATION_USERNAME,
  password: process.env.CONVERSATION_PASSWORD
  // url: process.env.CONVERSARION_URL
})

// Faço aqui o tratamento das mensagens....
const updateMessage = response => {
  var responseText = null
  if (!response.output) {
    response.output = {}
  }

  if (response.intents && response.intents[0]) {
    var intent = response.intents[0]
    // Se a confiança for baixa, é melhor enviar responstas para tirar a amibuidade.
    if (intent.confidence >= 0.75) {
      responseText = 'Eu entendi que sua intenção era ' + intent.intent
    } else if (intent.confidence >= 0.5) {
      responseText = 'Eu acho que sua intenção era ' + intent.intent
    } else {
      responseText = 'Eu não entendi sua intenção, por favor, reformule a frase'
    }
  }
  response.output.text = responseText
  return response
}

let context = null
let intent = null

const chat = app => {
  app.route('/send')
    .post(async (req, res) => {
      const msg = (req.body.text).trim()
      if (msg.length > 2) {
        return res.status(400).json({err: 'Entrada de texto muito grande'})
      }

      let payload = {
        workspace_id: process.env.WORKSPACE_ID,
        context: context || {},
        input: req.body || {},
        intent: intent || null
      }

      await conversation.message(payload, (err, data) => {
        if (err) {
          return res.status(err.statusCode || 400).json(err)
        } else {
          context = data.context
          intent = data.intents[0]
          return res.json(updateMessage(data))
        }
      })
    })
}

module.exports = chat
