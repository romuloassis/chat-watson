# BOOT AGENDAMENTO LOCAL

#### INSTALAR , CONFIGURAR E RODAR
**1 - Primeiro instalar um servidor http de forma global (para rodar o exemplo):**
`npm i -g http-server`

**2 - Instalar nodemon de forma global:**
`npm i -g nodemon`

**3 - Atualizar os pacotes:**
`npm install`

**4 - Subir os serviços:**

_**4.1** Exemplo do chat_ 
`npm run start-example`

_**4.2** O servidor_
`npm run start`

**5 - Abrir o chat:**
`http://localhost:8081`


####DOC WATSON: 
_`https://www.ibm.com/watson/developercloud/conversation/api/v1/node.html?node`_
