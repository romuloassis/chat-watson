require('dotenv').config()

const express = require('express')
const bodyParser = require('body-parser')
const services = require('./services')

const app = express()

app.use(express.static('./public')) // load UI from public folder

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

services(app)

app.listen(process.env.APP_PORT, () => {
  console.log(`RODANDO NA PORTA ${process.env.APP_PORT}`)
})
