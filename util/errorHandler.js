const errorHandler = (error, msg, code = 400, extras = {}) => {
  let response = { statusCode: code, message: msg, error: error, extras: extras }
  console.error(response)
  return response
}

module.exports = errorHandler
