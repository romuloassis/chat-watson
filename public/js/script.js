/* eslint-disable */

var Message = function (arg) {
  this.text = arg.text, this.message_side = arg.message_side

  this.draw = function (_this) {
    return function () {
      
      addMessage('right', _this.text)

      $.ajax({
        url: 'http://localhost:3000/send',
        type: 'POST',
        dataType: 'json',
        contentType: "application/json",
        data: JSON.stringify({text: this.text}), 
        success: function(data) {
          $.map(data.output.text, function (text) {
            addMessage('left', text)
          })
        },
        error: function(err) {
          console.log(err)
        }
      })
    }
  }(this)
  return this
}

var addMessage = function (side, text) {
  var $message = $($('.message_template').clone().html())
  $message.addClass(side).find('.text').html(text)
  $('.messages').append($message)
  $message.addClass('appeared')
  $('.messages').animate({ scrollTop: $('.messages').prop('scrollHeight') }, 300)
}

$(function () {
  var getMessageText = function () {
    var $message_input = $('.message_input')
    return $message_input.val()
  }

  var sendMessage = function (text) {
    var $messages, message
    if (text.trim() === '') {
      return
    }

    $('.message_input').val('')
    $messages = $('.messages')

    message = new Message({
      text: text,
      message_side: 'right'
    })
    
    return message.draw()
  }

  $('.send_message').click(function (e) {
    return sendMessage(getMessageText())
  })

  $('.message_input').keyup(function (e) {
    if (e.which === 13) {
      return sendMessage(getMessageText())
    }
  })

})